//
//  gitekAppDelegate.h
//  Guregas
//
//  Created by ikerib on 16/09/12.
//  Copyright (c) 2012 gitek. All rights reserved.
//

#import <UIKit/UIKit.h>

@class gitekViewController;

@interface gitekAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) gitekViewController *viewController;

@end
