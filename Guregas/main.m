//
//  main.m
//  Guregas
//
//  Created by ikerib on 16/09/12.
//  Copyright (c) 2012 gitek. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "gitekAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([gitekAppDelegate class]));
    }
}
