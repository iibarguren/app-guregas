//
//  gitekViewController.m
//  Guregas
//
//  Created by ikerib on 16/09/12.
//  Copyright (c) 2012 gitek. All rights reserved.
//

#import "gitekViewController.h"

@interface gitekViewController ()

@end

@implementation gitekViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSURL        *url       = [NSURL URLWithString:@"http://gitek2.grupogureak.com"];
    NSURLRequest *loadURL   = [[NSURLRequest alloc] initWithURL:url];
    
    [webView loadRequest:loadURL];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

@end
